# 前端代码说明

## 基础组件

## 编辑组件

## 数据组件

- [table](data/table.md)

## 布局组件

## util

## 重点代码结构

|Dir|说明|
|--|--|
|types|各类type目录, 包含与后端交互的payload结构|
|service|与后端提供的WebAPI对应|
|views|前端页面|