# x-table

基于element-plus table组件二次封装的数据表格组件, 具备常规的数据展示能力, 提供分页能力

## Props

|属性|类型|说明|默认值|
|--|--|--|--|
|table-columns|ITableColumn|列配置|[]|
|table-data|any[]|数据|[]|
|selection|any[]|被选中行**支持v-model双向绑定**|[]|
|show-pagination|boolean|是否显示分页控件|true|
|page-size|number|分页-每页显示数量|15|
|records-total|number|分页-记录数量|0|
|page-current|number|分页-当前页码|0|
|table-loading|boolean|是否正在加载数据|false|
|is-selection|boolean|是否允许选择|false|
|tooltips|boolean|是否根据内容长度自动显示tootips|true|
|is-single|boolean|是否仅允许|false|
|has-index|boolean||false|
|row-key|string|行key字段名称||
|stripe|boolean||true|
|border|boolean|是否显示边框|false|
|height|string\|number|表格高度|'100%'|

## Emits

|事件名称|说明|参数|
|--|--|--|
|selectionChange|选中行发生变化|`selection: any[]`|
|tableSelect|||
|**pageChange**|组件请求分页时|`page: number`|
|rowClick|当数据行被点击时|`row, column, event`|

## Methods

|方法名|说明|参数|
|--|--|--|
|toggleRowSelection|选择/取消选择指定行|`row:any,isSelected:boolean = false`|
|clearSelection|清空当前所有选中||
|setSelections|设置当前选中行|`rows: any[]`|

## Slots

|方法名|说明|传入参数|
|--|--|--|
|column.prop|可自定义列显示, 比如增加行内按钮, 或者数据 tag|`row`|

## ITableColumn Props

列配置, 通过json方式配置列

|属性|类型|说明|默认值|
|--|--|--|--|
|label|string|名称/显示文字||
|prop|string|属性名称||
|align?|'left'\|'center'\|'right'|水平方向对齐方式|'left'|
|width?|string\|number|列宽度||
|slot?|boolean|是否使用slot|false|
|fixed?|boolean|是否固定|false|
|minWidth?|number|最小列宽||
|formatter?|Function|格式化方法|`()=>{}`|