# 后端代码说明

项目总体基于微服务架构，技术实现基于`SpringCloudAlibaba`, 服务间调用使用`Dubbo`, 服务注册发现使用`Nacos`

- 开始之前请安装`MySQL`和`Nacos`
  - MySQL 5.7 以上兼容版本,建议使用 MariaDB 10.X及以上版本, 并在最终部署到用户环境前进行环境验证
- 根据项目用户和数据情况，可以选择分布式或者是单机进行部署
- 系统设计建议按照: 界面逻辑设计->数据库设计->接口设计->逻辑实现de 方式完成

## 代码结构

- x-view-server-starter: project-root
  - x-view-server-api: Interface 层, 提供所有`Service`接口及对应`model`定义
  - x-view-server-service: Service 实现层, 包含 DAO 层. 实现 Interface 层定义的所有接口，同时通过`mybatis`+`mybatis-plus`和数据库实现交互
  - x-view-server-web: Web 层, 为前端提供 WebAPI 服务, 提供 Swagger 方式注解
  - x-view-server-codegen: 代码生成器, 可以根据数据库设计生成

## 模块默认提供的WebAPI(基于代码生成器)

数据库表设计([设计要求](../database.md))完成后，使用框架提供的代码生成器，自动生成各层的基础代码，并提供常用的查增删改功能。后续根据实际逻辑需要进行扩展和调整.

前后端联调调用参照demo模块

### save: [PUT] 新增/修改

- 前端使用PUT方式调用
- 当传入主键字段(通常为id)时, 进行修改
- 未传入主键字段时, 做新增操作

### query: [POST] 分页查询

- 需要自行实现对应的query查询字段并在serviceImpl中进行实现

### delete: [DELETE] 单条删除

前端使用DELETE方式调用并传入id

### delete: [POST] 多条删除

前端使用POST方式调用，并传入id数组作为payload参数

## Tips

- 项目已集成`hutool`作为公共util, 在有util使用需求时，优先考虑使用`hutool`提供
- 建议使用`ID`字段作为主键, 并使用`objectId`, 使用`hutool: IdUtil.objectId()`生成
- 不要将MyBatisPlus的分页数据结构(`com.baomidou.mybatisplus.core.metadata.IPage`)通过直接通过service进行提供，因为dubbo无法将此数据结构进行序列化, 分页查询方法参照demo实现
- 由于 Web 层和 Service 实现层基于 dubbo 进行解耦分离，不要在 Web 层使用事务注解，以及复杂的逻辑实现