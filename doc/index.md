# x-view

x-view - For faster and simpler project starter.

项目旨在提供标准化、简单化的项目基础控件及脚手架。

[前端说明](fe/index.md) [后端说明](be/index.md)

[数据库设计规范](database.md)