### x-view

x-view - For faster and simpler project starter.

项目旨在提供标准化、简单化的项目基础控件及脚手架。

[Document](doc/index.md)

#### 基础功能

- 前后端脚手架
- 代码生成器
- 用户管理
- 资源（菜单）管理
- 角色权限管理

#### 项目构成

- x-view-server-starter: 后端脚手架 [说明](doc/be/be-index.md)
- x-view-web-starter: 前端脚手架

#### 快速开始

基础环境: MySQL,Nacos

##### 初始步骤

###### 后端

1. 初始化数据库

- 项目使用 MySQL 数据库
- 使用`sql`目录下的脚本初始化数据库`x_view`和`x_view_log`
  - x_view: 业务数据库
  - x_view_log: 存储访问日志
- 根据情况可以调整数据库名称

2. 打开后端项目`x-view-server-starter`并配置 mysql 及 nacos 连接(默认 localhost 可以不配置)
3. 启动后端 service 及 web 项目
4. _Optional_: 重命名 package 包适配对应项目

###### 前端

1. `cnpm i` -> `cnpm run dev`

#### 业务开发

1. database-first 进行数据库设计，参考[数据库设计规范](/doc/database.md)
2. 通过后端代码生成器生成基础代码

- `XViewCodeGenerator`
- 调整`namespace` `moduleName` `genTables` 等参数进行代码生成

3. 根据需求进行个性化定制
4. 前端参照`DemoTable`模块进行开发

#### 技术体系

##### x-view-server-starter 后端脚手架

- [spring-cloud-alibaba](https://sca.aliyun.com/zh-cn/) with [apache-dubbo](https://dubbo.apache.org/)
- [mybatis-plus](https://baomidou.com/)
- [nacos](https://nacos.io/)
- [hutool](https://hutool.cn/)

##### x-view-web-starter 前端脚手架

- [element-plus](https://element-plus.org/)
- [vue3](https://cn.vuejs.org/)
- [pinia](https://pinia.vuejs.org/)
- [unocss](https://unocss.dev/)
