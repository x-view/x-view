use x_view_log;
-- 创建schema
create table request_log
(
    ID           int auto_increment
        primary key,
    REQUEST_TIME timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP,
    REQUEST_PATH varchar(100)                        not null,
    USER_ID      char(32)                            null,
    REQUEST_IP   varchar(20)                         null,
    PARAMS       varchar(2000)                       null
);

create index IK_REQUEST_LOG_PATH
    on request_log (REQUEST_PATH);

create index IK_REQUEST_LOG_TIME
    on request_log (REQUEST_TIME);

create index IK_REQUEST_LOG_USER
    on request_log (USER_ID);

