-- 创建MySQL数据库，根据项目的情况进行创建
create schema x_view default charset utf8mb4 collate utf8mb4_general_ci;
-- 当前访问审计日志单独存储在一个schema里面，也可以自行适配存储到elasticsearch之类的
create schema x_view_log default charset utf8mb4 collate utf8mb4_general_ci;
