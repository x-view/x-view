use x_view;
-- 先删除原有的系统表
drop table if exists sys_dept;

drop table if exists sys_log_reg;

drop table if exists sys_resource;

drop table if exists sys_role;

drop table if exists sys_role_resource;

drop table if exists sys_user;

drop table if exists sys_user_role;

-- 创建schema
create table sys_dept
(
    ID          char(32)     not null
        primary key,
    PARENT_ID   char(32)     null,
    NAME        varchar(64)  not null,
    SHORT_NAME  varchar(64)  null,
    LEVEL_CODE  varchar(128) null,
    SORT        int          null,
    REMARK      varchar(128) null,
    STATUS      int          not null,
    DEPT_TYPE   int          not null,
    ORG_TYPE    varchar(20)  null comment '单位类型',
    CREATE_TIME timestamp    null,
    UPDATE_TIME timestamp    null,
    constraint UIX_SYS_DEPT_LEVEL_CODE
        unique (LEVEL_CODE)
);

create index IK_SYS_DEPT_ORG_TYPE
    on sys_dept (ORG_TYPE);

create index IX_SYS_DEPT_PARENT_ID
    on sys_dept (PARENT_ID);

create table sys_log_reg
(
    PATH varchar(100)  not null
        primary key,
    NAME varchar(50)   not null,
    TYPE int default 0 null comment '注册类型$0-绝对匹配 1-正则表达式 目前仅使用绝对匹配'
)
    comment '日志注册';

create table sys_resource
(
    ID            char(32)     not null
        primary key,
    PARENT_RES_ID char(32)     null,
    RES_NAME      varchar(64)  not null,
    RES_KEY       varchar(128) not null,
    RES_TYPE      varchar(16)  not null,
    RES_PATH      varchar(128) null,
    SORT          int          null,
    ICON          varchar(64)  null,
    CREATE_TIME   timestamp    null,
    UPDATE_TIME   timestamp    null,
    constraint IK_SYS_RESOURCE_KEY
        unique (RES_KEY)
);

create table sys_role
(
    ID        char(32)     not null
        primary key,
    ROLE_NAME varchar(64)  not null,
    ROLE_KEY  varchar(128) not null,
    SORT      int          null,
    REMARK    varchar(128) null,
    constraint UIX_SYS_ROLE_ROLE_KEY
        unique (ROLE_KEY)
);

create table sys_role_resource
(
    ROLE_ID varchar(32) not null,
    RES_ID  varchar(32) not null,
    primary key (ROLE_ID, RES_ID)
);

create table sys_user
(
    ID                   char(32)    not null
        primary key,
    USER_NAME            varchar(32) not null,
    REAL_NAME            varchar(32) null,
    PASSWD               varchar(32) not null,
    DEPT_ID              varchar(32) null,
    ACCESS_TYPE          varchar(32) null,
    MOBILE_NUMBER        varchar(32) null,
    EXP_DATE             date        null,
    INITIALIZED          int         not null,
    LOGIN_DEVICE_CONTROL int         null,
    ID_CODE              varchar(20) null,
    LAST_LOGON_TIME      timestamp   null,
    STATUS               int         not null,
    SORT                 int         null,
    ICON                 longtext    null,
    PASSWD_INIT          varchar(32) null,
    USER_TYPE            int         null,
    DUTY                 varchar(50) null,
    SEX                  int         null,
    CREATE_TIME          timestamp   null,
    UPDATE_TIME          timestamp   null,
    constraint UIX_SYS_USER_USER_ID
        unique (USER_NAME)
);

create table sys_user_role
(
    USER_ID char(32) not null,
    ROLE_ID char(32) not null,
    primary key (USER_ID, ROLE_ID)
);

-- 写入默认数据
INSERT INTO sys_dept (ID, PARENT_ID, NAME, SHORT_NAME, LEVEL_CODE, SORT, REMARK, STATUS, DEPT_TYPE, ORG_TYPE, CREATE_TIME, UPDATE_TIME) VALUES ('61d9906d212c85fa12afd7a3', null, 'X-View团队', 'X-View', '001', 1, null, 0, 1, null, null, '2022-01-08 12:11:38');
INSERT INTO sys_dept (ID, PARENT_ID, NAME, SHORT_NAME, LEVEL_CODE, SORT, REMARK, STATUS, DEPT_TYPE, ORG_TYPE, CREATE_TIME, UPDATE_TIME) VALUES ('61d9906d212c85fa12afd7a4', '61d9906d212c85fa12afd7a5', '公共控件组', '公共控件', '001002005', 1, null, 0, 0, null, null, '2021-10-04 06:41:41');
INSERT INTO sys_dept (ID, PARENT_ID, NAME, SHORT_NAME, LEVEL_CODE, SORT, REMARK, STATUS, DEPT_TYPE, ORG_TYPE, CREATE_TIME, UPDATE_TIME) VALUES ('61d9906d212c85fa12afd7a5', '61d9906d212c85fa12afd7a3', '前端团队', '前端', '001002', 1, null, 0, 1, null, null, '2021-12-26 07:26:27');
INSERT INTO sys_dept (ID, PARENT_ID, NAME, SHORT_NAME, LEVEL_CODE, SORT, REMARK, STATUS, DEPT_TYPE, ORG_TYPE, CREATE_TIME, UPDATE_TIME) VALUES ('61d9906d212c85fa12afd7a6', '61d9906d212c85fa12afd7a3', '后端团队', '后端', '001008', 4, null, 0, 1, null, null, '2021-10-04 06:40:50');
INSERT INTO sys_dept (ID, PARENT_ID, NAME, SHORT_NAME, LEVEL_CODE, SORT, REMARK, STATUS, DEPT_TYPE, ORG_TYPE, CREATE_TIME, UPDATE_TIME) VALUES ('61d9906d212c85fa12afd7a7', '61d9906d212c85fa12afd7a5', '快速实现组', 'Pro', '001002002', 2, null, 0, 0, null, null, '2021-10-04 06:41:56');
INSERT INTO sys_dept (ID, PARENT_ID, NAME, SHORT_NAME, LEVEL_CODE, SORT, REMARK, STATUS, DEPT_TYPE, ORG_TYPE, CREATE_TIME, UPDATE_TIME) VALUES ('61d9906d212c85fa12afd7a8', '61d9906d212c85fa12afd7a4', '设计组', '设计组', '001002005001', 1, '', 0, 0, null, '2021-10-02 12:54:09', '2021-10-04 07:27:47');
INSERT INTO sys_dept (ID, PARENT_ID, NAME, SHORT_NAME, LEVEL_CODE, SORT, REMARK, STATUS, DEPT_TYPE, ORG_TYPE, CREATE_TIME, UPDATE_TIME) VALUES ('61d9906d212c85fa12afd7a9', '61d9906d212c85fa12afd7a4', '实现组', '实现组', '001002005002', null, null, 0, 0, null, '2021-10-04 07:32:00', '2021-10-04 07:32:00');
INSERT INTO sys_log_reg (PATH, NAME, TYPE) VALUES ('/dept/query', '查询部门列表', 0);
INSERT INTO sys_log_reg (PATH, NAME, TYPE) VALUES ('/dept/tree', '单位部门树', 0);
INSERT INTO sys_log_reg (PATH, NAME, TYPE) VALUES ('/log/query', '日志查询', 0);
INSERT INTO sys_log_reg (PATH, NAME, TYPE) VALUES ('/login/check', '登录', 0);
INSERT INTO sys_log_reg (PATH, NAME, TYPE) VALUES ('/resource/menu', '获取菜单', 0);
INSERT INTO sys_log_reg (PATH, NAME, TYPE) VALUES ('/user/query', '查询用户列表', 0);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('61d9896b212c8bea507a6fec', null, '系统管理', 'sys', 'directory', '/sys', 9, 'Setting', null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('61d9896b212c8bea507a6fed', '61d9896b212c8bea507a6fec', '单位部门', 'sys.dept', 'menu', '/sys/dept/SysDept', 1, 'OfficeBuilding', null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('61d9896b212c8bea507a6fee', '61d9896b212c8bea507a6fed', '新增', 'sys.dept.add', 'function', null, null, null, null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('61d9896b212c8bea507a6fef', '61d9896b212c8bea507a6fed', '修改', 'sys.dept.edit', 'function', null, null, null, null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('61d9896b212c8bea507a6ff0', '61d9896b212c8bea507a6fed', '禁用', 'sys.dept.disable', 'function', null, null, null, null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('61d9896b212c8bea507a6ff1', '61d9896b212c8bea507a6fec', '用户管理', 'sys.user', 'menu', '/sys/user/SysUser', 2, 'User', null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('61d9896b212c8bea507a6ff2', '61d9896b212c8bea507a6ff1', '新增', 'sys.user.add', 'function', null, 1, null, null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('61d98c37212cecbf8416611a', '61d9896b212c8bea507a6ff1', '修改', 'sys.user.edit', 'function', null, 2, null, null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('61d98c37212cecbf8416611b', '61d9896b212c8bea507a6ff1', '启用', 'sys.user.enable', 'function', null, 5, null, null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('61d98c37212cecbf8416611c', '61d9896b212c8bea507a6ff1', '禁用', 'sys.user.disable', 'function', null, 4, null, null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('61d98c37212cecbf8416611f', '61d9896b212c8bea507a6ff1', '初始化密码', 'sys.user.resetPassword', 'function', null, 6, null, null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('61d98c37212cecbf84166120', '61d9896b212c8bea507a6ff1', '授权角色', 'sys.user.savePermission', 'function', null, 8, null, null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('61d98c37212cecbf84166121', '61d9896b212c8bea507a6fec', '权限资源', 'sys.resource', 'menu', '/sys/resource/SysResource', 3, 'Operation', null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('61d98c37212cecbf84166122', '61d98c37212cecbf84166121', '新增', 'sys.resource.add', 'function', null, null, null, null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('61d98c37212cecbf84166123', '61d98c37212cecbf84166121', '修改', 'sys.resource.edit', 'function', null, null, null, null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('61d98cd6212ce10b2cefa578', '61d98c37212cecbf84166121', '删除', 'sys.resource.delete', 'function', null, null, null, null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('61d98cd6212ce10b2cefa579', '61d9896b212c8bea507a6fec', '角色管理', 'sys.role', 'menu', '/sys/role/SysRole', 4, 'Unlock', null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('61d98cd6212ce10b2cefa57a', '61d98cd6212ce10b2cefa579', '新增', 'sys.role.add', 'function', null, 1, null, null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('61d98cd6212ce10b2cefa57b', '61d98cd6212ce10b2cefa579', '修改', 'sys.role.edit', 'function', null, 2, null, null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('61d98cd6212ce10b2cefa57c', '61d98cd6212ce10b2cefa579', '删除', 'sys.role.delete', 'function', null, 3, null, null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('61d98cd6212ce10b2cefa57e', '61d98cd6212ce10b2cefa579', '授权', 'sys.role.savePermission', 'function', null, 4, null, null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('61d98cd6212ce10b2cefa57f', '61d9896b212c8bea507a6fec', '操作日志', 'sys.auditlog', 'menu', '/sys/log/RequestLog', 5, 'Document', null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('61da2f83212ca5dadbff999c', '61d9896b212c8bea507a6fed', '启用', 'sys.dept.enable', 'function', null, null, null, null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('6352a706480ed1d5fd286182', '6353978e480e0497d832ac3a', '测试目录L2', 'test-dir', 'directory', '/test/testl2/', 9, 'ElementPlus', null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('6352a734480ed1d5fd286183', '6352a706480ed1d5fd286182', '十个字三级菜单1', 'test-l3', 'menu', '/test/testM1/TestM1', 1, 'Plus', null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('63536077480ed1d5fd286185', '6352a706480ed1d5fd286182', '十个字三级菜单2', 'test2', 'menu', '/test/testM2/TestM2', 2, 'FullScreen', null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('6353978e480e0497d832ac3a', null, '测试模块', 'test', 'directory', '/test/', 4, 'SwitchFilled', null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('6353abba480e9a951e0576e8', '6353978e480e0497d832ac3a', '测试二级模块', 'test3', 'menu', null, 2, 'ToiletPaper', null, null);
INSERT INTO x_view.sys_resource (ID, PARENT_RES_ID, RES_NAME, RES_KEY, RES_TYPE, RES_PATH, SORT, ICON, CREATE_TIME, UPDATE_TIME) VALUES ('6515411cbb81775d6b7983c9', '6353978e480e0497d832ac3a', '演示模块', 'demo.demo', 'menu', '/test/demo/DemoTable', 1, 'Lollipop', null, null);
INSERT INTO sys_role (ID, ROLE_NAME, ROLE_KEY, SORT, REMARK) VALUES ('61d9896b212c8bea507a6fe9', '超级管理员', 'ROLE_ADMIN', null, '超级角色，不可修改，不可随意分配');
INSERT INTO sys_role (ID, ROLE_NAME, ROLE_KEY, SORT, REMARK) VALUES ('61d9896b212c8bea507a6fea', '默认角色', 'ROLE_LOGIN', null, '登陆成功自动获取此角色，不可修改，不需要授予用户此角色，用户登录后自动拥有。可授予此角色一些通用的权限。');
INSERT INTO sys_user (ID, USER_NAME, REAL_NAME, PASSWD, DEPT_ID, ACCESS_TYPE, MOBILE_NUMBER, EXP_DATE, INITIALIZED, LOGIN_DEVICE_CONTROL, ID_CODE, LAST_LOGON_TIME, STATUS, SORT, ICON, PASSWD_INIT, USER_TYPE, DUTY, SEX, CREATE_TIME, UPDATE_TIME) VALUES ('61d9906d212c85fa12afd7aa', 'admin', '管理员', 'fe18ee796482d3256f7a8352480851c1', '61d9906d212c85fa12afd7a3', null, '13333331233', '2020-11-28', 0, null, '519999', null, 0, 966, null, null, 1, null, null, null, '2021-12-26 07:52:23');
INSERT INTO sys_user_role (USER_ID, ROLE_ID) VALUES ('61d9906d212c85fa12afd7aa', '61d9896b212c8bea507a6fe9');

drop table if exists x_view.demo_table;
-- 增加demo表
create table x_view.demo_table
(
    ID             varchar(26)   not null primary key,
    NAME           varchar(32)   not null comment '名称',
    TEXT_FIELD1   varchar(100)  null comment '字符串字段1',
    TEXT_FIELD2   varchar(100)  null comment '字符串字段2',
    NUMBER_FIELD1 INT           null comment '数字字段1',
    NUMBER_FIELD2 DECIMAL(9, 2) null comment '数字字段2',
    REMARK         varchar(1024) null comment '备注'
)
    charset = utf8mb4 comment 'Demo演示表';